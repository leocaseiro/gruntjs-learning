module.exports = function(grunt) {
		 
	// configure grunt on init
	grunt.initConfig({
	    // All files which will be convert to check with JSHint (Helps to detect errors and potential problems in code)
	    jshint: { … },

	    // All files which will be concat
	    concat: { … },

	    // All files which will be minify
	    uglify: { … },

	    // Tasks will be made using watch
	    watch: { … }
	});

	// Load plugins which have tasks specifieds on package.json
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task which will be runned without parameters
	grunt.registerTask('default', ['jshint', 'concat', 'uglify']);

};